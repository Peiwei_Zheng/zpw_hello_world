FROM hub.xtalpi.cc/xcsp/conda3_env:latest
ARG platform
LABEL MAINTAINER="peiwei.zheng@xtalpi.com"
LABEL VERSION="1.0.0"

WORKDIR /code
COPY . /code

# use new conda env
ENV CONDA_DEFAULT_ENV=zpw_hello_world
ENV CONDA_PREFIX="/root/miniconda3/envs/$CONDA_DEFAULT_ENV"
ENV PATH="$CONDA_PREFIX/bin:$PATH"

RUN apt update && apt install cron && apt clean

RUN conda env create -q -v -n $CONDA_DEFAULT_ENV -f environment.yml && \
    conda clean --all --yes && \
    # remove code from base image
    rm -rf /code

WORKDIR /code/hello
CMD ["python", "manager.py"]